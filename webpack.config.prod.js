var webpack = require('webpack');
module.exports = {
  entry: [
    './src/index.js'
  ],
  devtool: 'cheap-module-source-map',
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['react', 'es2015', 'stage-1']
      }
    }, { test: /\.css$/, loader: "style-loader!css-loader" }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.css'],
    modulesDirectories: [
      'node_modules'
    ]
  },
  plugins: [
    new webpack.DefinePlugin({ // <-- key to reducing React's size
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.DedupePlugin(), //dedupe similar code 
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    }), //minify everything
    new webpack.optimize.AggressiveMergingPlugin()//Merge chunks 
  ],
  devServer: {
    historyApiFallback: true,
    contentBase: './',
    disableHostCheck: true,
    host:'0.0.0.0',
    port: 9009
  }
};
