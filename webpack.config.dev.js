var webpack = require('webpack');
// var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
module.exports = {
  entry: [
    './src/index.js'
  ],
  devtool: 'eval-source-map',
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['react', 'es2015', 'stage-1']
      }
    }, { test: /\.css$/, loader: "style-loader!css-loader" }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.css'],
    modulesDirectories: [
      'node_modules'
    ]
  },
  // plugins: [
  //   new BundleAnalyzerPlugin()
  // ],
  devServer: {
    historyApiFallback: true,
    contentBase: './',
    disableHostCheck: true,
    host:'0.0.0.0',
    port: 9008
  }
};
