import * as firebase from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyCa-4vV3yUXpSZvqu07zepTTQYFta7Hwkw",
  authDomain: "digitalspacesinc-627f9.firebaseapp.com",
  databaseURL: "https://digitalspacesinc-627f9.firebaseio.com",
  projectId: "digitalspacesinc-627f9",
  storageBucket: "",
  messagingSenderId: "167451245842"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const auth = firebase.auth();

export {
  auth,
};