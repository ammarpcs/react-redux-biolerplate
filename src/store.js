import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk from 'redux-thunk';

//import root reducers
import rootReducer from './reducers';

//create an object for default data
const defaultState = {
};

//Create store method requires two params: 1- root reducer & 2 - default state
const store = createStore(rootReducer, defaultState, composeWithDevTools(applyMiddleware(ReduxThunk)));

export default store;