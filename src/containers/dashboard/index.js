import React, { Component } from 'react';
import { Route, Link, withRouter } from 'react-router-dom';

//import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Loader from '../loader';
import Header from '../header';
import Footer from '../footer';
import history from '../../utils/history';

class Dashboard extends Component {
  constructor(props) {
    super(props);

  }

  componentWillMount() {
  }

  componentDidMount() {
  }

  render() {

    return (
      <div>
        <Header history={this.props.history} />
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <h1>Dashboard is here</h1>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Dashboard;




