import React, {Component} from 'react';

export default class Empty extends React.Component{
    render(){
        return(
            <div className="container-fluid text-center">
              <h2>Sorry! No data is available</h2>
            </div>
        );
    }
}