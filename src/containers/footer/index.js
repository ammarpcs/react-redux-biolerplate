import React, { Component } from 'react';
import './index.css';

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="container-fluid">
          <div className="row justify-content-center">
            &copy; 2018 Digital Spaces Inc.
          </div>
        </div>
      </footer>
    );
  }
}




