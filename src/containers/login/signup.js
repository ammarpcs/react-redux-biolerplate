import React, { Component } from 'react';
import './index.css';

import { Route, Link, withRouter } from 'react-router-dom';
import { auth } from '../../firebase';
import firebase from 'firebase';

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
});

class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.state = { ...INITIAL_STATE };
    }

    onSubmit= (event) => {
        const {
            username,
            email,
            passwordOne,
            passwordTwo
        } = this.state;

        const {
            history,
        } = this.props;

        auth.doCreateUserWithEmailAndPassword(email, passwordOne)
            .then(authUser => {
                this.setState(() => ({ ...INITIAL_STATE }));
                this.props.history.push('/dashboard');

                // let user = firebase.auth().currentUser;
                // user.updateProfile({
                //     displayName: username,
                //   }).then(function() {
                //     this.props.history.push('/dashboard');
                //   }).catch(function(error) {
                //     console.log(error);
                //   });
            })
            .catch(error => {
                this.setState(byPropKey('error', error));
                console.log(error);
            });

        event.preventDefault();

    }
    render() {

        let {
            username,
            email,
            passwordOne,
            passwordTwo,
            error,
        } = this.state;

        return (

            <div className="login-form">
                <div className="container">
                    <div className="logo">
                        <h4 className="inline white">DSI Create Account</h4>
                        <Link to='/'>
                            <button type="button" className="login-page-btn inline" >Log In</button>
                        </Link>
                    </div>
                    <div className="login-item">
                        <form onSubmit={this.onSubmit} className="form form-login">

                            <div className="form-field">
                                <label className="user" htmlFor="login-username">
                                    <span className="hidden">Username</span>
                                </label>
                                <input
                                    value={username}
                                    onChange={event => this.setState(byPropKey('username', event.target.value))}
                                    type="text"
                                    placeholder="Full Name"
                                    className="form-input" />
                            </div>

                            <div className="form-field">
                                <label className="user" htmlFor="login-username">
                                    <span className="hidden">Username</span>
                                </label>
                                <input
                                    value={email}
                                    onChange={event => this.setState(byPropKey('email', event.target.value))}
                                    type="text"
                                    placeholder="Email Address"
                                    className="form-input"
                                />
                            </div>

                            <div className="form-field">
                                <label className="lock" htmlFor="login-password">
                                    <span className="hidden">Password</span>
                                </label>
                                <input
                                    value={passwordOne}
                                    onChange={event => this.setState(byPropKey('passwordOne', event.target.value))}
                                    type="password"
                                    placeholder="Password"
                                    className="form-input" />
                            </div>

                            <div className="form-field">
                                <label className="lock" htmlFor="login-password">
                                    <span className="hidden">Password</span>
                                </label>
                                <input
                                    value={passwordTwo}
                                    onChange={event => this.setState(byPropKey('passwordTwo', event.target.value))}
                                    type="password"
                                    placeholder="Confirm Password"
                                    className="form-input" />
                            </div>

                            <div className="form-field">
                                <input type="submit" value="Sign Up" />
                            </div>
                            {error && <p>{error.message}</p>}

                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default Signup;