import React, { Component } from 'react';
import { connect } from 'react-redux';
import history from '../../utils/history';
import  firebase from 'firebase';

export default function(ComposedComponent) {
  class Authentication extends Component {
    componentWillMount() {
      if (!this.props.loggedIn) {
        history.push('/');
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.loggedIn) {
        history.push('/');
      }
    }

    render() {
      if (!this.props.loggedIn) return <div></div>;
      
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return { loggedIn:  state.loggedIn };
  }

  return connect(mapStateToProps)(Authentication);
}