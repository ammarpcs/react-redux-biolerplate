import React, { Component } from 'react';

import { Route, Link, withRouter } from 'react-router-dom';
import history from '../../utils/history';
// import * as firebase from 'firebase';

// import { isLoggedIn } from '../../firebase/auth';
// import { updateLoggedIn } from '../../actions/loggedInActionCreator';
// import { bindActionCreators } from 'redux';
// import { connect } from 'react-redux';

class Login extends React.Component {

    constructor() {
        super();
    }

    componentWillMount() {
        // if (this.props.loggedIn === false) {
        //     return (
        //         history.push('/signin')
        //     );
        // }
        // else if (this.props.loggedIn == true) {
        //     return (
        //         history.push('/dashboard')
        //     );
        // }
    }

    render() {

        return (
            <div>
                <Link to="/dashboard">
                    <button type="button" className="btn btn-outline-primary ">
                        Go To Dashboard
                    </button>
                </Link>
            </div>
        );
    }
}


// function mapStateToProps(state) {
//     return {
//         loggedIn: state.loggedIn
//     }
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         actions: {
//             updateLoggedIn: bindActionCreators(updateLoggedIn, dispatch)
//         }
//     }
// }

// export default connect(mapStateToProps, null)(Login);
export default Login;