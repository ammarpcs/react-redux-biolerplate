import React, { Component } from 'react';
import './index.css';

import { Route, Link, withRouter } from 'react-router-dom';
import { auth } from '../../firebase';

import history from '../../utils/history'

import { updateLoggedIn } from '../../actions/loggedInActionCreator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { newAxios } from '../../services/dsi-api';

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
});

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
};

class SignIn extends React.Component {
    constructor() {
        super();
        this.state = { ...INITIAL_STATE };
    }

    componentWillMount() {
        if (this.props.loggedIn == true) {
            history.push('/dashboard');
        }

    }

    onSubmit = (event) => {
        const {
            email,
            password,
        } = this.state;

        const {
            history,
        } = this.props;

        auth.doSignInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState(() => ({ ...INITIAL_STATE }));
                this.props.actions.updateLoggedIn(true);
                sessionStorage.setItem('loggedIn', true);

                let customerName = email.split('@');
                customerName = customerName[0];
                sessionStorage.setItem('customerName', customerName);

                history.push('/dashboard');

            })
            .catch(error => {
                this.setState(byPropKey('error', error));
            });

        event.preventDefault();
    }

    render() {

        let {
            email,
            password,
            error,
        } = this.state;

        return (

            <div>
                <div className="img-logo">
                    <img src="/assets/img/logo.png" width="180" height="100" alt="DSI" />
                </div>
                <div className="login-form text-center">
                    <div className="container">
                        <div className="logo">
                            <h3 className="inline white">DSI Login</h3>
                        </div>

                        <div className="login-item">
                            <form onSubmit={this.onSubmit} className="form form-login">
                                <div className="form-field">
                                    <label className="user m-0" htmlFor="login-username">
                                        <span className="hidden">Username</span>
                                    </label>
                                    <input
                                        value={email}
                                        onChange={event => this.setState(byPropKey('email', event.target.value))}
                                        type="text"
                                        placeholder="Email Address"
                                        className="form-input">
                                    </input>
                                </div>

                                <div className="form-field">
                                    <label className="lock m-0" htmlFor="login-password">
                                        <span className="hidden">Password</span>
                                    </label>
                                    <input
                                        value={password}
                                        onChange={event => this.setState(byPropKey('password', event.target.value))}
                                        type="password"
                                        placeholder="Password"
                                        className="form-input">
                                    </input>
                                </div>

                                <div className="form-field pt-3">
                                    <input type="submit" value="Sign In" />
                                </div>
                                {error && <p>{error.message}</p>}
                            </form>

                            {/* <Link to='/signup'>
                            <div className="form-field">
                                <input type="button" value="Sign Up" />
                            </div>
                        </Link> */}

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        loggedIn: state.loggedIn
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: {
            updateLoggedIn: bindActionCreators(updateLoggedIn, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);