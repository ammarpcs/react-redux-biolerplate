import React, { Component } from 'react';

export default class Loader extends React.Component{

    render(){
        return (
            <div id="page-loader">
            <div className="loader"></div>
          </div>
        )
    }
}