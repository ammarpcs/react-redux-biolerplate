import React, { Component } from 'react';

import { Route, Link, withRouter } from 'react-router-dom';
import history from '../../utils/history';

// import { doSignOut } from '../../firebase/auth';
// import auth from '../../firebase';


// import { updateLoggedIn } from '../../actions/loggedInActionCreator';
// import { bindActionCreators } from 'redux';
// import { connect } from 'react-redux';

import './index.css';

class Header extends Component {

  constructor() {
    super();

    this.signOut = this.signOut.bind(this);
  }

  signOut() {

    let that = this;

    // doSignOut()
    //   .then(function () {
    //     that.props.actions.updateLoggedIn(false);
    //     sessionStorage.setItem('loggedIn', false);
    //     sessionStorage.clear();
    //     that.props.history.push('/signin');
    //   })
    //   .catch(error => {
    //     console.log(error);
    //   });

    history.push('/');

  }

  render() {
    //let user = auth.currentUser;

    return (
      <header>
        <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light justify-content-between">
        <a className="navbar-brand" href="javascript:void(0)">
            <img src="/assets/img/logo.png" width="100" height="60" alt="DSI"/>
          </a>
          {/* <input className="text-center" type="text" value="Welcome Test" ></input> */}
          <button onClick={this.signOut} type="button" id="logoutId" className="btn btn-primary btn-sm">Logout</button>
        </nav>
      </header>
    );
  }
}

// function mapDispatchToProps(dispatch) {
//   return {
//     actions: {
//       updateLoggedIn: bindActionCreators(updateLoggedIn, dispatch)
//     }
//   }
// }

// export default connect(null, mapDispatchToProps)(Header);

export default Header;