import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { Router, Route, Switch, browserHistory } from 'react-router-dom';

import store from './store';
import history from './utils/history';

import Login from './containers/login';
import Dashboard from './containers/dashboard';
//import authwrapper from './containers/login/auth-wrapper';

ReactDOM.render(
  <Provider store={store}>
    <div>
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={Login} />
          {/* <Route path="/dashboard" component={authwrapper(Dashboard)} /> */}
          <Route path="/dashboard" component={Dashboard} />
        </Switch>
      </Router>
    </div>
  </Provider>
  , document.querySelector('#main'));
