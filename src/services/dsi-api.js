//Api code wil go over here
import axios from 'axios';

export default axios.create({
    baseURL: `http://18.216.208.225:3000/v1`,
    headers: {
        'Access-Control-Allow-Headers': '*',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
        
    }
});